package action_modules;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_objects.HomePage;
import page_objects.ProductsFoundListPage;

/**
 * Created by clovis_olivier on 29/05/2018.
 */
public class SortByIncreasingPriceAction {
    public static void Execute(WebDriver driver) throws Exception{

        WebDriverWait wait = new WebDriverWait(driver, 10);

        Select sortMenu = new Select(ProductsFoundListPage.sortMenu());
        sortMenu.selectByValue("price-asc-rank");

    }
}
