package page_objects;

import helpers.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by clovis_olivier on 29/05/2018.
 */
public class HomePage extends BasePage {
    private static WebElement element = null;

    public HomePage(WebDriver driver ){
        super(driver);
    }

    public static WebElement searchBar() throws Exception{
        try{
            element = driver.findElement(By.id("twotabsearchtextbox"));
            Log.info("Search Bar is found on the Home Page");
        }catch (Exception e){
            Log.error("Search Bar is not found on the Home Page");
            throw(e);
        }
        return element;
    }
}
