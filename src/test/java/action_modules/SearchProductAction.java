package action_modules;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_objects.HomePage;

import java.util.HashMap;
import java.util.List;

/**
 * Created by clovis_olivier on 29/05/2018.
 */
public class SearchProductAction {
    public static void Execute(WebDriver driver, String searchKey) throws Exception{

        WebDriverWait wait = new WebDriverWait(driver, 10);

        WebElement searchBar = HomePage.searchBar();

        wait.until(ExpectedConditions.elementToBeClickable(searchBar));

        searchBar.sendKeys(searchKey);
        searchBar.sendKeys(Keys.RETURN);
    }
}
