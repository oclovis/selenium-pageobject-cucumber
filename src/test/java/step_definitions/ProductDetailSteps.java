package step_definitions;

import action_modules.AddElementInBasketAction;
import cucumber.api.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import page_objects.ProductDetailPage;
import page_objects.ProductsFoundListPage;

/**
 * Created by clovis_olivier on 29/05/2018.
 */
public class ProductDetailSteps {
    public WebDriver driver;

    public ProductDetailSteps(){
        driver = Hooks.driver;
    }

    @Then("^Add element in the basket$")
    public void add_element_in_the_basket() throws Throwable {
        PageFactory.initElements(driver, ProductDetailPage.class);

        AddElementInBasketAction.Execute(driver);
    }
}
