package page_objects;

import helpers.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by clovis_olivier on 29/05/2018.
 */
public class ProductDetailPage extends BasePage {

    private static WebElement element = null;

    private static List<WebElement> elements = null;

    public ProductDetailPage(WebDriver driver ){
        super(driver);
    }

    public static WebElement addInBasketButton() throws Exception{
        try{
            element = driver.findElement(By.id("add-to-cart-button-ubb"));
            Log.info("add-to-cart-button found on the Products list Page");
        }catch (Exception e){
            Log.error("add-to-cart-button not found on the Products list Page");
            throw(e);
        }
        return element;
    }

}
