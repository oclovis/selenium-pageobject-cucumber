package step_definitions;

import action_modules.*;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import helpers.Log;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import page_objects.HomePage;
import page_objects.ProductsFoundListPage;

/**
 * Created by clovis_olivier on 29/05/2018.
 */
public class ProductSelectionSteps {
    public WebDriver driver;

    public ProductSelectionSteps(){
        driver = Hooks.driver;
    }

    @Given("^I open Amazon homepage$")
    public void i_open_Amazon_homepage() throws Throwable {
        Log.info("I open Amazon homepage");
        driver.get("https://www.amazon.com");
    }

    @And("^I search for \"([^\"]*)\" in the search bar$")
    public void i_search_for_value_in_the_search_bar(String arg1) throws Throwable {
        Log.info("I set "+arg1+" in the search bar$");

        PageFactory.initElements(driver, HomePage.class);

        SearchProductAction.Execute(driver, arg1);
    }

    @Then("^I check products are listed$")
    public void i_check_products_are_listed() throws Throwable {
        Log.info("I check products are listed");

        PageFactory.initElements(driver, ProductsFoundListPage.class);
        CheckProductsAreListedAction.Execute(driver);
    }

    @Then("^Sort by increasing price$")
    public void sort_by_increasing_price() throws Throwable {
        PageFactory.initElements(driver, ProductsFoundListPage.class);
        SortByIncreasingPriceAction.Execute(driver);
    }

    @Then("^Consult first product detail$")
    public void consult_first_product_detail() throws Throwable {
        //PageFactory.initElements(driver, ProductsFoundListPage.class);
        ConsultProductDetailAction.Execute(driver);
    }


}
