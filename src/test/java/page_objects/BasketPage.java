package page_objects;

import helpers.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by clovis_olivier on 29/05/2018.
 */
public class BasketPage extends BasePage {


    private static WebElement element = null;

    public BasketPage(WebDriver driver ){
        super(driver);
    }

    public static WebElement alertSuccess() throws Exception {
            try{
                element = driver.findElement(By.className("a-alert-success"));
                Log.info("a-alert-success found on the Basket Page");
            }catch (Exception e){
                Log.error("a-alert-success not found on the Basket Page");
                throw(e);
            }
            return element;

    }
}
