package step_definitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;

import helpers.Log;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.apache.log4j.xml.DOMConfigurator;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

/**
 * Created by clovis_olivier on 27/05/2018.
 */
public class Hooks {

    public static WebDriver driver;

    @Before
    public void setUp() throws MalformedURLException{

        DOMConfigurator.configure("log4j.xml");

        Log.startTestCase();

        driver = new ChromeDriver();
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @After
    public void tearDown(){
        Log.endTestCase();
        driver.quit();
    }
}
