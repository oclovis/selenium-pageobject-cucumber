package action_modules;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_objects.ProductDetailPage;
import page_objects.ProductsFoundListPage;

import java.util.List;

/**
 * Created by clovis_olivier on 29/05/2018.
 */
public class AddElementInBasketAction {
    public static void Execute(WebDriver driver) throws Exception{

        WebDriverWait wait = new WebDriverWait(driver, 10);

        WebElement firstProduct = ProductDetailPage.addInBasketButton();
        wait.until(ExpectedConditions.elementToBeClickable(firstProduct));

        firstProduct.click();
    }
}
