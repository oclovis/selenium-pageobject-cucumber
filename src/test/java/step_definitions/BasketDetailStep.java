package step_definitions;

import action_modules.AddElementInBasketAction;
import action_modules.CheckBasketContainOneElementAction;
import cucumber.api.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import page_objects.ProductDetailPage;

/**
 * Created by clovis_olivier on 29/05/2018.
 */
public class BasketDetailStep {
    public WebDriver driver;

    public BasketDetailStep(){
        driver = Hooks.driver;
    }

    @Then("^An element is in the basket$")
    public void add_element_in_the_basket() throws Throwable {
        PageFactory.initElements(driver, BasketDetailStep.class);

        CheckBasketContainOneElementAction.Execute(driver);
    }

}
