package action_modules;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_objects.HomePage;
import page_objects.ProductsFoundListPage;

import java.util.List;

/**
 * Created by clovis_olivier on 29/05/2018.
 */
public class CheckProductsAreListedAction {
    public static void Execute(WebDriver driver) throws Exception{

        WebDriverWait wait = new WebDriverWait(driver, 10);

        List<WebElement> productsList = ProductsFoundListPage.listProduct();

        assert(productsList.size()>3);

    }
}
