Feature: Search product

  Scenario: Search for new product
    Given I open Amazon homepage
    When I search for "DVD" in the search bar
    Then I check products are listed
    And Sort by increasing price
    Then Consult first product detail
    And Add element in the basket
    And An element is in the basket




