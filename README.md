# selenium-pageobject-cucumber

Test automation project POC

# Maven

This project use Maven as dependency manager.

# Requirements

The Webdriver 'chromedriver' should be installer. See https://sites.google.com/a/chromium.org/chromedriver/

# Install and run

git clone https://oclovis@bitbucket.org/oclovis/selenium-pageobject-cucumber.git 
<br/>cd selenium-pageobject-cucumber
<br/>mvn clean install


# Execution reports

The testing execution generate reports in file ./target

# Execution Log

Log of the execution are stored in file ./logfile.log

# Exploratory test

See file Explorqtory test session Confluence page.pdf