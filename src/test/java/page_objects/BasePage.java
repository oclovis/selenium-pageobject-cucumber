package page_objects;

import org.openqa.selenium.WebDriver;

/**
 * Created by clovis_olivier on 27/05/2018.
 */
public abstract class BasePage {
    public static WebDriver driver;
    public static boolean bResult;

    public BasePage(WebDriver driver){
        BasePage.driver = driver;
        BasePage.bResult = true;
    }

}
