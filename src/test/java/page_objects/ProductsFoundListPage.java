package page_objects;

import helpers.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by clovis_olivier on 29/05/2018.
 */
public class ProductsFoundListPage extends BasePage {


    private static WebElement element = null;

    private static List<WebElement> elements = null;

    public ProductsFoundListPage(WebDriver driver ){
        super(driver);
    }

    public static List<WebElement> listProduct() throws Exception{
        try{
            elements = driver.findElements(By.className("s-result-item"));
            Log.info("Products list found on the Products list Page");
        }catch (Exception e){
            Log.error("Products list found on the Products list Page");
            throw(e);
        }
        return elements;
    }

    public static WebElement firstProductDetailLink() throws Exception{
        try{
            element = driver.findElement(By.xpath("//*[@id=\"result_2\"]/div/div/div/div[2]/div[1]/div/a"));
            Log.info("First Product found on the Products list Page");
        }catch (Exception e){
            Log.error("First Product found on the Products list Page");
            throw(e);
        }
        return element;
    }

    public static WebElement sortMenu() throws Exception{
        try{
            element = driver.findElement(By.id("sort"));
            Log.info("Sort menu found on the Products list Page");
        }catch (Exception e){
            Log.error("Sort menu found on the Products list Page");
            throw(e);
        }
        return element;
    }


}
